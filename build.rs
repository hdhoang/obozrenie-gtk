use std::process::Command;

fn main() {
    let status = Command::new("glib-compile-resources")
        .args(&["--generate", "resources.xml"])
        .current_dir("src/resources")
        .status()
        .unwrap();
    if !status.success() {
        panic!(println!("Resource compilation failed with status {}", status))
    }
}
