extern crate obozrenie_core as core;
extern crate gio;
extern crate gdk_pixbuf;
extern crate gtk;
extern crate std;

use std::sync::Arc;
use std::borrow::Borrow;
use std::collections::HashMap;

use gdk_pixbuf::Pixbuf;

use gio::ApplicationExt;

use gtk::DialogExt;
use gtk::WidgetExt;
use gtk::WindowExt;

type LoggerFunc = Arc<Fn(&str)>;

fn show_about_dialog<T>(parent: &T, logo: Option<&Pixbuf>) where T: gtk::IsA<gtk::Window> {
    let dialog = gtk::AboutDialog::new();

    dialog.set_program_name("Obozrenie");
    dialog.set_license_type(gtk::License::Gpl30);
    dialog.set_transient_for(Some(parent));
    dialog.set_logo(logo);
    dialog.run();
    dialog.hide();
}

pub struct Application {

    pub logger: LoggerFunc,
    pub application: gtk::Application,
    pub main_window: gtk::ApplicationWindow,
    pub refresh_button: gtk::Button,
}

impl Application {
pub fn new(logger: LoggerFunc, builder: &gtk::Builder, gtk_app: gtk::Application, icons: HashMap<String, Arc<Pixbuf>>, logo: Arc<Pixbuf>) -> Application {
    let refresh_button: gtk::Button = builder.get_object("refresh_button").unwrap();
    
    let main_window : gtk::ApplicationWindow = builder.get_object("main_window").unwrap();
    main_window.show_all();

    { let mw = main_window.clone(); gtk_app.connect_startup(move |gapp| { gapp.add_window(&mw); }); }
    { let mw = main_window.clone(); let l = logo.clone(); gtk::ButtonExt::connect_clicked(&refresh_button.clone(), move |_| { show_about_dialog(&mw, Some(Borrow::<Pixbuf>::borrow(&l))); }); }

    Application {logger: logger, refresh_button: refresh_button, application: gtk_app, main_window: main_window } 
}

pub fn start(&self) {
    self.application.run(0, &[]);
}
}
