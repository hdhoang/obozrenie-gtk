use std::io::Write;
use std::sync::Arc;

extern crate obozrenie_core as core;

extern crate gdk_pixbuf;
extern crate gio;
extern crate gtk;

mod application;
mod resources;


fn main() {
    resources::init();
    let gtk_app = gtk::Application::new(Some("io.obozrenie"), gio::APPLICATION_FLAGS_NONE).unwrap();

    let icons = resources::get_game_icons();
    //let mut core = core::Core::new();
    //core.read_game_lists(resources::get_default_config());
    let mut app = application::Application::new(Arc::new(|v: &str| { std::io::stdout().write(format!("{}\n", v).as_bytes()); }), &resources::get_builder(), gtk_app, icons, Arc::new(resources::get_application_icon()));
    app.start();
}
