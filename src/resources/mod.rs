extern crate gdk_pixbuf;
extern crate gio;
extern crate gio_sys;
extern crate glib;
extern crate glib_sys;
extern crate gtk;
extern crate libc;
extern crate std;

use std::collections::HashMap;
use std::sync::Arc;

macro_rules! RESOURCES_FILE {() => { "resources.gresource" }}
macro_rules! RESOURCES_PREFIX {() => { "/io/obozrenie" }}

pub fn get_game_icons() -> HashMap<String, Arc<gdk_pixbuf::Pixbuf>> {
    let mut hm = HashMap::new();
    for k in vec!["freeminer",
                  "hl1mp",
                  "hl2mp",
                  "minetest",
                  "openttd",
                  "portal2",
                  "rtcw",
                  "tf",
                  "warsow"] {
        match gdk_pixbuf::Pixbuf::new_from_resource(&format!("{}/{}.svg",
                                                             concat!(RESOURCES_PREFIX!(),
                                                                     "/game_icons"),
                                                             k)) {
            Ok(v) => hm.insert(k.to_string(), Arc::new(v)),
            _ => continue,
        };
    }
    hm
}

pub fn get_default_config() -> String {
    include_str!("game_lists.json").to_string()
}

pub fn get_builder() -> gtk::Builder {
    gtk::Builder::new_from_string(include_str!("obozrenie_gtk.ui"))
}

pub fn get_application_icon() -> gdk_pixbuf::Pixbuf {
    gdk_pixbuf::Pixbuf::new_from_resource(concat!(RESOURCES_PREFIX!(), "/obozrenie-short.svg"))
        .unwrap()
}

pub fn init() {
    let res_bytes = include_bytes!(RESOURCES_FILE!());

    unsafe {
        // gbytes and resource will not be freed
        let gbytes = glib_sys::g_bytes_new(res_bytes.as_ptr() as *const libc::c_void,
                                           res_bytes.len());
        let resource = gio_sys::g_resource_new_from_data(gbytes, std::ptr::null_mut());
        gio_sys::g_resources_register(resource);
    }
}
